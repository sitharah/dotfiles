#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto -a'
# PS1='[\u@\h \W]\$ '
alias get='sudo pacman -S'
alias update='sudo pacman -Su'
alias syncdb='sudo pacman -Su --noconfirm'
alias upgrade='sudo pacman -Syu --noconfirm'
alias rem='sudo pacman -Rs'
alias clean='sudo pacman -Sc'
alias neo='neofetch'
alias vi='vim'
alias nv='nvim'
