# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=0
SAVEHIST=0
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/robot/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
alias vi='vim'
alias get='sudo pacman -S'
alias update='sudo pacman -Su --noconfirm'
alias syncdb='sudo pacman -Sy'
alias upgrade='sudo pacman -Syu --noconfirm'
alias neo='neofetch'
alias ls='ls -a --color=auto'
alias rem='sudo pacman -Rs'
alias clean='sudo pacman -Sc'
